package Entities;

import Entities.AssuranceDommage;
import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: DommageCollision
 *
 */
@Entity

public class DommageCollision extends AssuranceDommage implements Serializable {

	
	private static final long serialVersionUID = 1L;

	public DommageCollision() {
		super();
	}
   
}
