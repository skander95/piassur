package Entities;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: portfeuille
 *
 */
@Entity
@Table(name="portfeuilles")
public class Portfeuille implements Serializable {

	private static final long serialVersionUID = -558553967080513790L;

	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY )
	@Column(name="Id")
	private int id;
	@OneToOne(mappedBy="portfeuille")
	private Client client; 
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy="portfeuilles")
	private Set<Transactions> Transactions;
	
	
	public Portfeuille() {
		super();
	}
   
}
