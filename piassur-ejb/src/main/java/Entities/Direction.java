package Entities;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Direction
 *
 */
@Entity
@Table(name="directions")
public class Direction implements Serializable {

	private static final long serialVersionUID = -558553967080513790L;

	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY )
	@Column(name="Id")
	private int id;
	private Type_Nom nom;

	
	@OneToMany(cascade = CascadeType.ALL, mappedBy="directions")
	private Set<Employe> employe;
	
	
	
	public Direction() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Type_Nom getNom() {
		return nom;
	}

	public void setNom(Type_Nom nom) {
		this.nom = nom;
	}
	
   
}
