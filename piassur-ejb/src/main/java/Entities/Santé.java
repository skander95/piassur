package Entities;

import Entities.AssurancePersonne;
import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Santé
 *
 */
@Entity

public class Santé extends AssurancePersonne implements Serializable {

	
	private static final long serialVersionUID = 1L;

	public Santé() {
		super();
	}
   
}
