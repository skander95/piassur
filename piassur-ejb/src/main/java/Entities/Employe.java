package Entities;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Employe
 *
 */


@Entity
@Table(name="employes")
public class Employe implements Serializable {

	private static final long serialVersionUID = -558553967080513790L;

	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY )
	@Column(name="Id")
	private int id;
	private String nom;
	private String prenom;
	private String motdepasse;
	private String Login;
	
	
	@ManyToOne
	FilialBancaire filialbancaires ; 
	
	
	@ManyToOne
	Direction directions ;
	
	
	
	
	
	
	public Employe() {
		super();
	}




	public int getId() {
		return id;
	}




	public void setId(int id) {
		this.id = id;
	}




	public String getNom() {
		return nom;
	}




	public void setNom(String nom) {
		this.nom = nom;
	}




	public String getPrenom() {
		return prenom;
	}




	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}




	public String getMotdepasse() {
		return motdepasse;
	}




	public void setMotdepasse(String motdepasse) {
		this.motdepasse = motdepasse;
	}




	public String getLogin() {
		return Login;
	}




	public void setLogin(String login) {
		Login = login;
	}
	
	
	
	
	
	
	
	
	
   
}
