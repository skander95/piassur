package Entities;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Assurance
 *
 */
@Entity
@Table(name="assurances")
public class Assurance implements Serializable {

	private static final long serialVersionUID = -558553967080513790L;

	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY )
	@Column(name="Id")
	private int id;
	@ManyToMany(mappedBy="assurances", cascade = CascadeType.ALL)
	private Set<Compte> comptes;
	
	public Assurance() {
		super();
	}
   
}
