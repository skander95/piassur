package Entities;

import Entities.Assurance;
import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: AssuranceDommage
 *
 */
@Entity

public class AssuranceDommage extends Assurance implements Serializable {

	
	private static final long serialVersionUID = 1L;

	public AssuranceDommage() {
		super();
	}
   
}
