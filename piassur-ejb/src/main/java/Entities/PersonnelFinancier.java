package Entities;

import Entities.Employe;
import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: PersonnelFinancier
 *
 */
@Entity

public class PersonnelFinancier extends Employe implements Serializable {

	
	private static final long serialVersionUID = 1L;

	public PersonnelFinancier() {
		super();
	}
   
}
