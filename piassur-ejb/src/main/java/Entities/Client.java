package Entities;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * Entity implementation class for Entity: Client
 *
 */
/**
 * @author Skander
 *
 */
@Entity
@Table(name="clients")
public class Client implements Serializable {

	private static final long serialVersionUID = -558553967080513790L;

	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY )
	@Column(name="Id")
	private int id;

   private String nom;
   private String prénom;
   private String E_mail;
   private int tel;
   private Portfeuille portf;
   
   
   
   @OneToOne
   private Portfeuille portfeuille; 
   
   
   
   @OneToMany(cascade = CascadeType.ALL, mappedBy="clients")
   private Set<Compte> compte;

   
public Portfeuille getPortf() {
	return portf;
}
public void setPortf(Portfeuille portf) {
	this.portf = portf;
}
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getNom() {
	return nom;
}
public void setNom(String nom) {
	this.nom = nom;
}
public String getPrénom() {
	return prénom;
}
public void setPrénom(String prénom) {
	this.prénom = prénom;
}
public String getE_mail() {
	return E_mail;
}
public void setE_mail(String e_mail) {
	E_mail = e_mail;
}


public int getTel() {
	return tel;
}
public void setTel(int tel) {
	this.tel = tel;
}

   
}
