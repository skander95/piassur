package Entities;

import Entities.Compte;
import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Compte_Courant
 *
 */
@Entity

public class Compte_Courant extends Compte implements Serializable {

	
	private static final long serialVersionUID = 1L;

	public Compte_Courant() {
		super();
	}
   
}
