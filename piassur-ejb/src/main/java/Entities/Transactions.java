package Entities;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Transactions
 *
 */
@Entity
@Table(name="transactions")
public class Transactions implements Serializable {

	private static final long serialVersionUID = -558553967080513790L;

	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY )
	@Column(name="Id")
	private int id;
	
	@ManyToOne
	Portfeuille portfeuilles; 

	public Transactions() {
		super();
	}
   
}
