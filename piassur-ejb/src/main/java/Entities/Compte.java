package Entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Entity implementation class for Entity: Compte
 *
 */
@Entity

@Table(name="Compte")
public class Compte implements Serializable {

	private static final long serialVersionUID = -558553967080513790L;

	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY )
	@Column(name="Id")
	private int id;
	private String Rib;
	private String Etat;
	private double montant; 
	private Date date_creation;
	private Type_compte type;
	
	

	
	@ManyToOne
	private Client clients; 
	
	
	@ManyToOne
	private FilialBancaire filialebancaire; 
	
	@ManyToMany(cascade = CascadeType.ALL)
	private Set<Assurance> assurances; 
	
	
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getRib() {
		return Rib;
	}
	public void setRib(String rib) {
		Rib = rib;
	}
	public String getEtat() {
		return Etat;
	}
	public void setEtat(String etat) {
		Etat = etat;
	}
	public double getMontant() {
		return montant;
	}
	public void setMontant(double montant) {
		this.montant = montant;
	}
	public Date getDate_creation() {
		return date_creation;
	}
	public void setDate_creation(Date date_creation) {
		this.date_creation = date_creation;
	}
	public Type_compte getType() {
		return type;
	}
	public void setType(Type_compte type) {
		this.type = type;
	}
	
	
   
}
