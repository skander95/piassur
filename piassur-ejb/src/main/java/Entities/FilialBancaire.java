package Entities;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: FilialBancaire
 *
 */
@Entity
@Table(name="filialeBancaire")
public class FilialBancaire implements Serializable {

	private static final long serialVersionUID = -558553967080513790L;

	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY )
	@Column(name="Id")
	private int id;
	
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy="filialbancaires")
	private Set<Employe> employes;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy="filialebancaire")
	private Set<Compte> comptes;

	public FilialBancaire() {
		super();
	}
   
}
