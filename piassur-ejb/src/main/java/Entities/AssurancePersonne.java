package Entities;

import Entities.Assurance;
import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: AssurancePersonne
 *
 */
@Entity

public class AssurancePersonne extends Assurance implements Serializable {

	
	private static final long serialVersionUID = 1L;
	private Type_souscription sous;

	public AssurancePersonne() {
		super();
	}

	public Type_souscription getSous() {
		return sous;
	}

	public void setSous(Type_souscription sous) {
		this.sous = sous;
	}
   
}
