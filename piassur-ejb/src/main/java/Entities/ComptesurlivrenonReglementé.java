package Entities;

import Entities.Compte_Epargne;
import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: ComptesurlivrenonReglementé
 *
 */
@Entity

public class ComptesurlivrenonReglementé extends Compte_Epargne implements Serializable {

	
	private static final long serialVersionUID = 1L;

	public ComptesurlivrenonReglementé() {
		super();
	}
   
}
